# veriffcar: A car renting application

## Summary / TLDR;

- Used [yasinaydinnet/assignment-base](https://github.com/yasinaydinnet/assignment-base) project as base template. 
- Source code is at: [yasinaydinnet/veriffcar](https://github.com/yasinaydinnet/veriffcar)
- Version control system (VCS) is **git**. Tried to use **micro commits** as possible. 
- To test it better in development, **GET** HTTP method is used. In production, I would use **PUT** or **POST** depending on the context. 
- Some suggestions I have found throught this task are located at [doc/Suggestions](./doc/Suggestions.md) file.
- Using GitHub Actions for CI/CD. 
- Demo is on Heroku at https://veriffcar.herokuapp.com/

## Documentation

All documentations are in [/doc](./doc/) folder:
- [App requirements](./doc/Requirements.md)
- [How to run locally (develop)](./doc/Development.md)
- [How to publish (production)](./doc/Production.md)
- [Some Technical Details](./doc/TechDetails.md)
- [3rd Party Links/Resources Used](./doc/Links.md)
- [Suggestions from my side](./doc/Suggestions.md)

## Demo

Application is running live at: https://veriffcar.herokuapp.com/

## Things left

From the requirements:
- Action (finishing renting process) after the verification is complete
- (Optional) Sign up / Log in functionality
- (Optional) Add some API tests to the suite
- (Optional) Add tests, CI flow and test coverage

Personal:
- Improve UI/UX
- Separate frontend and backend
- Convert hosted Vue.js instance to transpiled
