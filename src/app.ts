require("express-async-errors");
import express from "express";
import * as apiController from "./controllers/api";
import * as carController from "./controllers/car";

const app = express();

// Static UI
app.use("/", express.static("./public"));

// API
app.get("/api/car/list", carController.list);
app.get("/api/car/resetData", carController.resetData);
app.get("/api/car/:carId/rent", carController.rent);
app.get("/api/car/:carId/return", carController.returnCar);
app.use(apiController.catchErrors);

export default app;
