export const enum CarStatus {
  available = "available",
  rented = "rented",
  pendingVerification = "pendingVerification",
}

interface Car {
  id: string;
  status: CarStatus;
  make: string;
  model: string;
  year: number;
  plate: string;
  vin: string;
  pictureUrl: string;
}
