type ApiResponse = any;

interface BaseApiResponseBody {
  status: string;
  statusCode: number;
}

interface ApiResponseBodyError extends BaseApiResponseBody{
  error: string;
}

interface ApiResponseBodySuccess extends BaseApiResponseBody{
  data: any;
}
