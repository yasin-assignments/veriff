interface VeriffVerificationResponse {
  id: string;
  url: string;
  vendorData: string;
  host: string;
  status: string;
  sessionToken: string;
}

interface VeriffPerson {
  firstName?: string;
  lastName?: string;
  idNumber?: string;
  gender?: string;
}

declare enum VeriffDocumentType {
  PASSPORT, ID_CARD, DRIVERS_LICENSE, RESIDENCE_PERMIT
}

interface VeriffDocument {
  number?: string;
  country?: string;
  type?: VeriffDocumentType;
}

interface VeriffVerificationRequest {
  callback?: string;
  person?: VeriffPerson;
  vendorData?: string;
  lang?: string;
  timestamp: string;
}

interface VeriffVerificationRequestBody {
  verification: VeriffVerificationRequest;
}
