import * as carService from "../services/car";
import { Request, Response } from "express";
import { successResponse } from "./api";

export const resetData = async (req: Request, res: Response): Promise<void> => {
  await carService.resetData();
  successResponse(res, "ok");
};

export const list = async (req: Request, res: Response): Promise<void> => {
  const data2 = await carService.listCars();

  successResponse(res, data2);
};

export const rent = async (req: Request, res: Response): Promise<void> => {
  const carId = req.params.carId;

  const verification: VeriffVerificationResponse = await carService.rentCar(
    carId
  );
  console.log({ verification });

  successResponse(res, verification.url);
};

export const returnCar = async (req: Request, res: Response): Promise<void> => {
  const carId = req.params.carId;

  await carService.returnCar(carId);

  successResponse(res, "return");
};
