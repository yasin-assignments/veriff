import { Request, Response, NextFunction } from "express";

export const successResponse = (res: Response, data: ApiResponse): void => {
  res.type("application/json");
  res.header("Content-Type", "application/json");

  const body: ApiResponseBodySuccess = {
    status: "ok",
    statusCode: 200,
    data,
  };

  res.status(200).send(JSON.stringify(body));
};

export const catchErrors = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  res.type("application/json");
  res.header("Content-Type", "application/json");

  const statusCode = 400;

  const body: ApiResponseBodyError = {
    status: "error",
    statusCode,
    error: err.message,
  };

  res.status(statusCode).send(JSON.stringify(body));
  next();
};
