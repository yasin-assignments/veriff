process.on("uncaughtException", console.error);

import { createServer } from "http";
import app from "./app";

const PORT = process.env.PORT || 3333;
const ENV = process.env.NODE_ENV || "development";

const server = createServer(app);

server.listen(PORT, () => {
  if (ENV === "development") {
    console.log(`Server is ready on http://localhost:${PORT}`);
  }
});

server.on("error", (error) => {
  console.error(error);
});

export default server;
