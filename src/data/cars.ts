import { Car, CarStatus } from "../types/car.d.js";

const car1 = {
  id: "78ee8bcb-bcd0-4181-9246-fd392eb4a093",
  status: CarStatus.available,
  make: "Tesla",
  model: "Model S",
  year: 2019,
  plate: "001 TSL",
  vin: "1FAHP24W18G142522",
  pictureUrl:
    "https://www.cstatic-images.com/car-pictures/xl/usc80tsc032a021001.png",
};

const car2 = {
  id: "8a787926-13fe-43c4-aefa-558c25da4c5b",
  status: CarStatus.rented,
  make: "Porsche",
  model: "911 Carrera",
  year: 2020,
  plate: "105 PRS",
  vin: "4T1SK12E2SU417435",
  pictureUrl:
    "https://www.motortrend.com/uploads/sites/10/2019/12/2020-porsche-911-carrera-s-coupe-angular-front.png",
};

const getInitialData = (): Array<Car> => {
  return [{ ...car1 }, { ...car2 }];
};

export let cars: Array<Car> = getInitialData();

export const resetInitialData = () => {
  cars = getInitialData();
};
