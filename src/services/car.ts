import { cars, resetInitialData } from "../data/cars";
import { postSession } from "./veriff";
import { Car, CarStatus } from "../types/car";

export const listCars = async (): Promise<Array<Car>> => {
  return cars;
};

export const resetData = async (): Promise<void> => {
  resetInitialData();
  return;
};

export const rentCar = async (
  carId: string
): Promise<VeriffVerificationResponse> => {
  if (!carId) throw new Error("carId is missing");

  const car = cars.find((car) => car.id === carId);
  if (!car) throw new Error("car not found");

  if (car.status !== CarStatus.available) {
    throw new Error("car is not available for rent");
  }

  car.status = CarStatus.pendingVerification;

  const response: VeriffVerificationResponse = await postSession(carId);
  return response;
};

export const returnCar = async (carId: string): Promise<void> => {
  if (!carId) throw new Error("carId is missing");

  const car = cars.find((car) => car.id === carId);
  if (!car) throw new Error("car not found");

  if (car.status !== CarStatus.rented) throw new Error("car wasnt rented");

  car.status = CarStatus.available;

  return;
};
