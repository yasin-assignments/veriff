import axios, { AxiosResponse, AxiosRequestConfig, AxiosInstance } from "axios";

const BASE_URL: string =
  process.env.NODE_ENV === "production"
    ? "https://veriffcar.herokuapp.com/"
    : "https://localhost:3333";

const API_KEY = process.env.VERIFF_API_KEY;

const axiosSettings: AxiosRequestConfig = {
  baseURL: "https://stationapi.veriff.com/v1",
  headers: {
    "Content-Type": "application/json",
    "X-AUTH-CLIENT": API_KEY,
  },
};

const stationApi: AxiosInstance = axios.create(axiosSettings);

export const postSession = async (
  carId: string
): Promise<VeriffVerificationResponse> => {
  const now = new Date();

  const body: VeriffVerificationRequestBody = {
    verification: {
      callback: BASE_URL,
      person: { firstName: "Yasin", lastName: "Aydin" },
      vendorData: carId,
      timestamp: now.toISOString(),
    },
  };

  try {
    const response: AxiosResponse = await stationApi.post("/sessions", body);
    const verification: VeriffVerificationResponse = response.data.verification;

    return verification;
  } catch (e) {
    throw Error("veriff postSession failed: " + e.response.data.message);
  }
};
