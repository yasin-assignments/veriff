# Releasing to Production

## Via Heroku

Prequisites:
- Locally installed app (see docs/Development)
- Heroku-CLI (`npm install -g heroku-cli` or `yarn global add heroku-cli`)
- Docker

After cloning the project, locally:

```sh
heroku login
heroku container:login
heroku container:push web -a veriffcar
heroku container:release -a veriffcar web
```

After the deployment, app will be accessible from https://veriffcar.herokuapp.com/
