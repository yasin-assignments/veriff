# 3rd Party Links/Resources Used

- For skel/scaffolding: [microsoft/TypeScript-Node-Starter][1]
- Node.js .gitignore: [github/gitignore][2]
- [Node-TS-Docker][3]

Veriff:
- [developers.veriff.com - API Documentation][4]
- [station.veriff.com - Control panel][4]
- [alchemy.veriff.com - Embedded Verification (DOESNT WORK WITHOUT VALID URL)][5]

[1]: https://github.com/microsoft/TypeScript-Node-Starter/tree/master/src
[2]: https://github.com/github/gitignore/blob/master/Node.gitignore
[3]: https://github.com/microsoft/TypeScript-Node-Starter/pull/263/files
[4]: https://developers.veriff.com/
[5]: https://alchemy.veriff.com/
[5]: https://station.veriff.com/
