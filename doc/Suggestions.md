# Suggestions / Questions

1. In examples at https://developers.veriff.com/#sessions , **request**  module is used. However this package is [deprecated](http://npmjs.org/package/request) and should not be suggested for use anymore. [Axios](https://www.npmjs.com/package/axios) could be a great alternative

2. On **Integrations Config** I saw a **Public Key** and **Private Key**. In the [API docs][docs] I have never seen a usage for **Private Key**. Also there are no mentions of "private" or "public" words. 

3. In [API docs][docs] for method **POST:/session/** It doesn't say what is the **timestamp** field for. I tried setting it to a date in 1992 and everything was still working.

4. In page https://station.veriff.com/integrations/cba42fcf-c359-4577-a2f1-3f08881ee675/settings , in **Settings** type, there is a typo: `Check ceritifcate`

5. In the example at https://developers.veriff.com/#validating-x-signature it specifies `curl --request POST 'http://localhost:3001/verification/' -k`, however when I tried to create URL with callback URL with **http** (not https), it says `Only HTTPS return URLs are allowed.` The example is misleading. 

6. In addition to **5.**, during the setting the integration (https://station.veriff.com/integrations/cba42fcf-c359-4577-a2f1-3f08881ee675/settings), I think localhost should be enabled for test/local environments. HTTPS check could be ignored for local hosts and loopback IPs.

7. Another typo in https://developers.veriff.com/#validating-x-signature : `webhook listnener`. Should be `webhook listener`.

8. I could not find any TypeScript types for Veriff API (Like `@types/veriff`). I created `src/types/veriff.d.ts` for my self usage and as an example. 

9. When I try a broken URL for verification like https://alchemy.veriff.com/v/wrong , instead of getting an error like 404, I got the error `This session got old and expired.`. I would use a 404 page instead, to provide more obfuscation. 

[docs]: https://developers.veriff.com/

