# Requirements: Full Stack Engineer Test Task

We want you to try integrating Verif as our clients do. Imagine that you’re building car renting application and you need to verify people that want to rent a car.

Build an application to rent a car and integrate Verif into it.

## FEATURES

In application user should be able:
- See the list of cars available
- Choose the car and see its specification
- Rent the car
- User identity should be verified via Verif. If approved, car should be rented, if declined, the request should be declined
- Return the car

We will create account for you inside Station and you can find technical documentation on integrating Verif on developers.verif.com

## REQUIREMENTS

We expect the application to be written in JavaScript. Other than that – no limitations. You may choose whatever you see fit. We will be glad if you could give insight on why you have chosen specific technology Please, deploy application and upload all of your code to GitHub.

## OPTIONAL BONUS TASKS
1. Add Sign up / Log in functionality
2. Use DevTools to check what API endpoints are used to create session, add some API tests to the suite
3. Cover you code with tests and create CI flow with coverage measurement
